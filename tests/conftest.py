from pathlib import Path

import pytest

FIXTURES_PATH = Path(__file__).parent.resolve() / "fixtures"


@pytest.fixture(scope="module")
def fixtures_path() -> Path:
    yield FIXTURES_PATH
