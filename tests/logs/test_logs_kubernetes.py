import datetime
import json
from pathlib import Path

import pytest
from requests_mock import Mocker

from toolforge_weld.kubernetes import K8sClient
from toolforge_weld.kubernetes_config import fake_kube_config
from toolforge_weld.logs.kubernetes import KubernetesSource
from toolforge_weld.logs.source import LogEntry


@pytest.fixture
def fake_kubernetes_client() -> K8sClient:
    return K8sClient(
        kubeconfig=fake_kube_config(),
        user_agent="fake",
        timeout=5,
    )


def test_KubernetesSource_query(
    fake_kubernetes_client: K8sClient, requests_mock: Mocker, fixtures_path: Path
):
    requests_mock.get(
        "/api/v1/namespaces/tool-test/pods?labelSelector=app.kubernetes.io%2Fcomponent%3Dweb",
        json=json.loads(
            (fixtures_path / "logs" / "kubernetes" / "pod-list.json").read_text()
        ),
    )

    requests_mock.get(
        "/api/v1/namespaces/tool-test/pods/test-797f8cc5c-lx74z/log?container=webservice&follow=False&pretty=True&timestamps=True&tailLines=10",
        text=(fixtures_path / "logs" / "kubernetes" / "log.txt").read_text().strip(),
    )

    log_source = KubernetesSource(client=fake_kubernetes_client)

    assert list(
        log_source.query(
            selector={"app.kubernetes.io/component": "web"}, follow=False, lines=10
        )
    ) == [
        LogEntry(
            pod="test-797f8cc5c-lx74z",
            container="webservice",
            datetime=datetime.datetime(
                2023, 6, 22, 9, 47, 31, 794474, tzinfo=datetime.timezone.utc
            ),
            message="first",
        ),
        LogEntry(
            pod="test-797f8cc5c-lx74z",
            container="webservice",
            datetime=datetime.datetime(
                2023, 6, 22, 9, 47, 32, 36482, tzinfo=datetime.timezone.utc
            ),
            message="second",
        ),
        LogEntry(
            pod="test-797f8cc5c-lx74z",
            container="webservice",
            datetime=datetime.datetime(
                2023, 6, 22, 9, 47, 32, 370956, tzinfo=datetime.timezone.utc
            ),
            message="third",
        ),
        LogEntry(
            pod="test-797f8cc5c-lx74z",
            container="webservice",
            datetime=datetime.datetime(
                2023, 6, 22, 9, 47, 32, 403615, tzinfo=datetime.timezone.utc
            ),
            message="fourth",
        ),
        LogEntry(
            pod="test-797f8cc5c-lx74z",
            container="webservice",
            datetime=datetime.datetime(
                2023, 6, 22, 9, 47, 32, 464222, tzinfo=datetime.timezone.utc
            ),
            message="fifth",
        ),
        LogEntry(
            pod="test-797f8cc5c-lx74z",
            container="webservice",
            datetime=datetime.datetime(
                2023, 6, 22, 9, 47, 32, 510694, tzinfo=datetime.timezone.utc
            ),
            message="sixth",
        ),
        LogEntry(
            pod="test-797f8cc5c-lx74z",
            container="webservice",
            datetime=datetime.datetime(
                2023, 6, 22, 9, 47, 32, 557647, tzinfo=datetime.timezone.utc
            ),
            message="seventh",
        ),
        LogEntry(
            pod="test-797f8cc5c-lx74z",
            container="webservice",
            datetime=datetime.datetime(
                2023, 6, 22, 9, 47, 32, 560241, tzinfo=datetime.timezone.utc
            ),
            message="eight",
        ),
        LogEntry(
            pod="test-797f8cc5c-lx74z",
            container="webservice",
            datetime=datetime.datetime(
                2023, 6, 22, 9, 47, 32, 752871, tzinfo=datetime.timezone.utc
            ),
            message="ninth",
        ),
        LogEntry(
            pod="test-797f8cc5c-lx74z",
            container="webservice",
            datetime=datetime.datetime(
                2023, 6, 22, 9, 47, 32, 822658, tzinfo=datetime.timezone.utc
            ),
            message="tenth",
        ),
    ]


def test_KubernetesSource_query_no_pods(
    fake_kubernetes_client: K8sClient, requests_mock: Mocker, fixtures_path: Path
):
    requests_mock.get(
        "/api/v1/namespaces/tool-test/pods?labelSelector=app.kubernetes.io%2Fcomponent%3Dweb",
        json=json.loads(
            (fixtures_path / "logs" / "kubernetes" / "pod-list-empty.json").read_text()
        ),
    )

    log_source = KubernetesSource(client=fake_kubernetes_client)
    assert (
        list(
            log_source.query(
                selector={"app.kubernetes.io/component": "web"}, follow=False, lines=10
            )
        )
        == []
    )
